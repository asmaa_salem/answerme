<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>لوحة التحكم| @yield('title')</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="{{Request::root()}}/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="{{Request::root()}}/assets/font-awesome/4.5.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="{{Request::root()}}/assets/css/fonts.googleapis.com.css" />
		<link rel="stylesheet" href="{{Request::root()}}/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
		<link rel="stylesheet" href="{{Request::root()}}/assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="{{Request::root()}}/assets/css/ace-rtl.min.css" />
		<script src="{{Request::root()}}/assets/js/ace-extra.min.js"></script>
		<link href="https://fonts.googleapis.com/css?family=Cairo:600" rel="stylesheet">
		



		<style type="text/css">
			h1,h2,h3,h4,h5,span,div,nav,label,input,button,table,tr,td{
				font-family: 'Cairo', sans-serif;
				color:#751a42;
			}

		</style>
		@yield('links')
	</head>
	<body class="no-skin rtl" >
		<div id="navbar" class="navbar navbar-default          ace-save-state" style="background-color: #751a42;">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" 
				style="background-color: #751a42; border:1px solid #fff " id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="navbar-header pull-right">
					<a href="index.html" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							   جاوبني
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-left" role="navigation">
						<i class="fa fa-sign-in fa-3x" style="color:#fff"></i>
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
							<h2>لوحة التحكم</h2>
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-info"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-info"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->

				<ul class="nav nav-list">
					<li class="active open">
						<a href="index.html">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> لوجة التحكم </span>
						</a>

						<b class="arrow"></b>
					</li>

			

				

					

				

				

					<li class="">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-file-o"></i>

							<span class="menu-text">
								Other Pages

								<span class="badge badge-primary">5</span>
							</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="active">
								<a href="blank.html">
									<i class="menu-icon fa fa-caret-right"></i>
									Blank Page
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>
				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
								 @yield('content')
					</div>
				</div>	
			</div>	

			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">جاوبني</span>
							 &copy; 2017-2018
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="{{Request::root()}}/assets/js/jquery-2.1.4.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{Request::root()}}/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="{{Request::root()}}/assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->
		<script src="{{Request::root()}}/assets/js/ace-elements.min.js"></script>
		<script src="{{Request::root()}}/assets/js/ace.min.js"></script>



		<!-- inline scripts related to this page -->
		 @yield('footer')
	</body>
</html>
