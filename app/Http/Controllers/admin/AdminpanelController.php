<?php

namespace AnswerMe\Http\Controllers\admin;

use Illuminate\Http\Request;
use AnswerMe\Http\Controllers\Controller;

class AdminpanelController extends Controller
{
	public function index(){
		return view('admin.home.index');
	}
}
